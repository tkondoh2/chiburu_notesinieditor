﻿#include "appendvalueto.h"

#include "parameters.h"
#include <CNExt/environment.h>
#include <CNExt/lmbcslist.h>

namespace cnx = CNExt;

void AppendValueTo::operator ()(Parameters &params)
{
  cnx::Lmbcs name = lmbcs(params.name());
  cnx::Lmbcs value = lmbcs(params.value());

  cnx::Lmbcs oldValue = cnx::GetEnvironment()(name);
  if (oldValue.isEmpty())
  {
    cnx::SetEnvironment()(name, value);
    return;
  }

  cnx::LmbcsList list = oldValue.split(",");
  if (!list.contains(value))
  {
    list.append(value);
    cnx::SetEnvironment()(name, list.join(","));
  }
}
