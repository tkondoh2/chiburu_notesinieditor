﻿#include "setvalue.h"

#include "parameters.h"
#include <CNExt/environment.h>

namespace cnx = CNExt;

void SetValue::operator ()(Parameters &params)
{
  cnx::SetEnvironment()(lmbcs(params.name()), lmbcs(params.value()));
}
