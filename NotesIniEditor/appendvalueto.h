﻿#ifndef APPENDVALUETO_H
#define APPENDVALUETO_H

#include "command.h"

class AppendValueTo
    : private Command
{
public:
  virtual void operator()(Parameters& params) override;
};

#endif // APPENDVALUETO_H
