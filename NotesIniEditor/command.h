﻿#ifndef COMMAND_H
#define COMMAND_H

class Parameters;

class Command
{
public:
  Command();

  virtual ~Command();

  virtual void operator()(Parameters &params) = 0;
};

#endif // COMMAND_H
