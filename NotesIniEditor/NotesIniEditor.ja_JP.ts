<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ja_JP">
<context>
    <name>QObject</name>
    <message>
        <location filename="parameters.cpp" line="20"/>
        <source>Select a command from the following items, &apos;set&apos;,&apos;unset&apos;,&apos;appendto&apos;,&apos;removefrom&apos;.</source>
        <oldsource>Select from the following operation items and specify, &apos;set&apos;,&apos;unset&apos;,&apos;appendto&apos;,&apos;removefrom&apos;.</oldsource>
        <translation>次の項目「set」、「unset」、「appendto」、「removefrom」からコマンドを選択します。</translation>
    </message>
    <message>
        <location filename="parameters.cpp" line="27"/>
        <source>Specify the name of variable.</source>
        <translation>変数の名前を指定します。</translation>
    </message>
    <message>
        <location filename="parameters.cpp" line="33"/>
        <source>Specify the value of variable.</source>
        <translation>変数の値を指定します。</translation>
    </message>
    <message>
        <location filename="parameters.cpp" line="73"/>
        <source>The value of the variable is not specified.</source>
        <translation>変数の値が指定されていません。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="61"/>
        <source>Do nothing.</source>
        <translation>何もしません。</translation>
    </message>
</context>
</TS>
