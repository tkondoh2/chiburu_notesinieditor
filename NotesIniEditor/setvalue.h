﻿#ifndef SETVALUE_H
#define SETVALUE_H

#include "command.h"

class SetValue
    : private Command
{
public:
  virtual void operator()(Parameters &params) override;
};

#endif // SETVALUE_H
