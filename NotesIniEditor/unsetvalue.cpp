﻿#include "unsetvalue.h"

#include "parameters.h"
#include <CNExt/environment.h>

namespace cnx = CNExt;

void UnsetValue::operator ()(Parameters &params)
{
  cnx::SetEnvironment()(lmbcs(params.name()), cnx::Lmbcs());
}
