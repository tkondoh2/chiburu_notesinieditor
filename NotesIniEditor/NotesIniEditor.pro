QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        main.cpp \
    setvalue.cpp \
    unsetvalue.cpp \
    appendvalueto.cpp \
    removevaluefrom.cpp \
    parameters.cpp \
    command.cpp

# 実行ファイル名を指定
TARGET = niniedit

HEADERS += \
    setvalue.h \
    unsetvalue.h \
    appendvalueto.h \
    removevaluefrom.h \
    parameters.h \
    command.h

INCLUDEPATH += $$PWD/../../CHIBURU_NotesExt
DEPENDPATH += $$PWD/../../CHIBURU_NotesExt
LIBS += -lnotes -lCNExt

win32 {
    DEFINES += W W32 NT
    contains(QMAKE_TARGET.arch, x86_64) {
        DEFINES += W64 ND64 _AMD64_
    }
}
else:macx {
    # MACのDWORDをunsigned int(4バイト)にする
    # DEFINES += MAC
    DEFINES += MAC LONGIS64BIT
}

TRANSLATIONS += NotesIniEditor.ja_JP.ts

DISTFILES += \
    NotesIniEditor.ja_JP.ts

RESOURCES += \
    notesinieditor.qrc
