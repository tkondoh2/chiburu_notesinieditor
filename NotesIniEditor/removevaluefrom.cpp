﻿#include "removevaluefrom.h"

#include "parameters.h"
#include <CNExt/environment.h>
#include <CNExt/lmbcslist.h>

namespace cnx = CNExt;

void RemoveValueFrom::operator ()(Parameters &params)
{
  cnx::Lmbcs name = lmbcs(params.name());
  cnx::Lmbcs value = lmbcs(params.value());

  cnx::Lmbcs oldValue = cnx::GetEnvironment()(name);
  if (oldValue.isEmpty())
    return;

  cnx::LmbcsList list = oldValue.split(",");
  list.removeAll(value);
  cnx::SetEnvironment()(name, list.join(","));
}
