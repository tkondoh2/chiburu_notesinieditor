﻿#ifndef REMOVEVALUEFROM_H
#define REMOVEVALUEFROM_H

#include "command.h"

class RemoveValueFrom
    : private Command
{
public:
  virtual void operator()(Parameters &params) override;
};

#endif // REMOVEVALUEFROM_H
