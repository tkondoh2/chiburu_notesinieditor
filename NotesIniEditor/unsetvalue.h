﻿#ifndef UNSETVALUE_H
#define UNSETVALUE_H

#include "command.h"

class UnsetValue
    : private Command
{
public:
  virtual void operator()(Parameters& params) override;
};

#endif // UNSETVALUE_H
