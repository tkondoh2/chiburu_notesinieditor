﻿#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <QTextStream>

enum class CommandType {
  Nothing
  , Set
  , Unset
  , AppendTo
  , RemoveFrom
};

class Parameters
{
public:
  Parameters();

  CommandType parse();
  bool checkValue() const;
  const QString &name() const;
  const QString &value() const;

private:
  QString name_;
  QString value_;
  mutable QTextStream qerr_;
};

inline const QString &Parameters::name() const
{
  return name_;
}

inline const QString &Parameters::value() const
{
  return value_;
}

#endif // PARAMETERS_H
