﻿#include <QCoreApplication>

#include <CNExt/api.h>
#include <QTimer>
#include <QTranslator>
#include <QLocale>

#include "parameters.h"
#include "setvalue.h"
#include "unsetvalue.h"
#include "appendvalueto.h"
#include "removevaluefrom.h"

namespace cnx = CNExt;

int main(int argc, char *argv[])
{
  // Notes APIを初期化する。
  cnx::Api api(argc, argv);

  // qAppを初期化する。
  QCoreApplication app(argc, argv);
  app.setOrganizationName("Chiburu Systems");
  app.setApplicationName("NotesIniEditor");
  app.setApplicationVersion("1.1.0");

  QString filePath = QString("NotesIniEditor.%1.qm")
      .arg(QLocale::system().name());
  QTranslator translator;
  if (translator.load(filePath, ":/translations"))
  {
    app.installTranslator(&translator);
  }

  QTimer::singleShot(0, []
  {
    // コマンドラインをパースする。
    Parameters params;
    switch (params.parse())
    {
    case CommandType::Set:
      SetValue()(params);
      break;

    case CommandType::Unset:
      UnsetValue()(params);
      break;

    case CommandType::AppendTo:
      AppendValueTo()(params);
      break;

    case CommandType::RemoveFrom:
      RemoveValueFrom()(params);
      break;

    default:
    {
      QTextStream s(stderr);
      s << QObject::tr("Do nothing.") << endl;
    }
      break;
    }
    QCoreApplication::exit(0);
  });

  // 終了(Notes APIも同時に終了処理)
  return app.exec();
}
