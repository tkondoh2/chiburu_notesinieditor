﻿#include "parameters.h"

#include <QCommandLineParser>

Parameters::Parameters()
  : qerr_(stderr)
{
}

CommandType Parameters::parse()
{
  // コマンドラインパーサ
  QCommandLineParser parser;
  parser.addVersionOption();
  parser.addHelpOption();

  // 位置引数「操作」
  parser.addPositionalArgument(
        "operation"
        , QObject::tr("Select a command from the following items,"
                      " 'set','unset','appendto','removefrom'.")
        );

  // 位置引数「変数名」
  parser.addPositionalArgument(
        "name"
        , QObject::tr("Specify the name of variable.")
        );

  // オプション引数「変数値」
  QCommandLineOption valueOption(
        QStringList({"V", "value"})
        , QObject::tr("Specify the value of variable.")
        , "value"
        );
  parser.addOption(valueOption);

  // パースの実行
  parser.process(*qApp);

  // 位置引数を取得
  QStringList args = parser.positionalArguments();
  name_ = args[1];
  value_ = parser.value(valueOption);

  // 操作ごとに定数を返す。
  if (args[0] == "set")
  {
    if (checkValue())
      return CommandType::Set;
  }
  else if (args[0] == "unset")
  {
    return CommandType::Unset; // Unsetのみ値がいらない
  }
  else if (args[0] == "appendto")
  {
    if (checkValue())
      return CommandType::AppendTo;
  }
  else if (args[0] == "removefrom")
  {
    if (checkValue())
      return CommandType::RemoveFrom;
  }
  return CommandType::Nothing;
}

bool Parameters::checkValue() const
{
  if (value_.isEmpty())
  {
    qerr_ << QObject::tr("The value of the variable is not specified.");
    return false;
  }
  return true;
}
